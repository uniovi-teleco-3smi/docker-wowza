# Contenedor con servidor de medios Wowza para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modifícalo según tus preferencias.

### Parámetros que pueden modificarse

 * Licencia de uso
 * Nombre del usuario administrador
 * Clave del usuario administrador
 * Puertos UDP para transmisión en directo

Los parámetros deben cambiarse en el fichero 'docker-compose.yml' antes de iniciar el contenedor.

### Arrancar el contenedor

Para iniciar el contenedor (ejecutar su imagen):

```console
sudo docker-compose up -d
```

Para parar/arrancar/reiniciar el contenedor:

```console
sudo docker-compose stop/start/restart
```

Puede arrancarse el contenedor sin necesidad de volúmenes comentando la sección _volumes_ del servicio.
